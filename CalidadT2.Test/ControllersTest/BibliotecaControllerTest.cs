﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
    public class BibliotecaControllerTest
    {
        [Test]
        public void listaBiblioteca()
        {
            Usuario usuario = new Usuario()
            {
                Id = 1,
                Nombres = "Luigui Mendoza",
                Password = "admin",
                Username = "admin"
            };

            var list = new Mock<IBibliotecaRepositorio>();
            var logea = new Mock<IUsuarioRepositorio>();

            list.Setup(o => o.listaBiblioteca(usuario)).Returns(new List<Biblioteca>());
            //logea.Setup(o => o.logeado()).Returns(usuario);

            var lista = new BibliotecaController(null, list.Object, logea.Object,null);

            var view = lista.Index();

            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}
