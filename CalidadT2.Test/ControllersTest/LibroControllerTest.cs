﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
    public class LibroControllerTest
    {

        [Test]
        public void DetalleId()
        {
            var Detalle = new Mock<ILibrosRepositorio>();


            Libro libro = new Libro()
            {
                Nombre = "La casa verde",
                Id = 1,
                Autor = new Autor()
                {
                    Id = 1,
                    Nombres = "Mario"
                },
                Comentarios = new List<Comentario>()
                {
                    new Comentario()
                    {
                        Id = 1,
                        Texto = "......"
                    }
                }
            };

            Detalle.Setup(o => o.libroId(1)).Returns(libro);

            var controller = new LibroController(null, Detalle.Object, null, null);

            var view = controller.Details(1);

            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}
