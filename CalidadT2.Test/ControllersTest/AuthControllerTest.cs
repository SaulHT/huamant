﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
   public  class AuthControllerTest
    {
        [Test]
        public void login()
        {
            var user = new Mock<IUsuarioRepositorio>();
            var htt = new Mock<IHttpContexRepositorio>();

            user.Setup(o => o.getUsuario("admin", "admin")).Returns(new Usuario { }); ;

            var controller = new AuthController(null, user.Object, htt.Object);
            var view = controller.Login("admin","admin");

            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
    }
}
