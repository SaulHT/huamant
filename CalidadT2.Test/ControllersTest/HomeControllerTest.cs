﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
   public  class HomeControllerTest
    {
        [Test]
        public void listaLibros()
        {
            var list = new Mock<ILibrosRepositorio>();

            list.Setup(o => o.listaLibros()).Returns(new List<Libro>());

            var controller = new HomeController(null,list.Object);

            var view = controller.Index();

            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}
