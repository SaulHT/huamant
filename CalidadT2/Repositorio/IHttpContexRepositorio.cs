﻿using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Repositorio
{
   public interface IHttpContexRepositorio
    {
        public void Login(ClaimsPrincipal claims);
        public void SetHttpContext(HttpContext context);
       // public void LogeadoUser(Claim claim);

       
    }
}
