﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Repositorio
{
   public interface IUsuarioRepositorio
    {
        Usuario getUsuario(string username, string password);

        Usuario logeado(Claim claim);
        //Usuario logeoUsuario();
    }
}
