﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorio
{
   public  interface ILibrosRepositorio
    {
        List<Libro> listaLibros();

        Libro libroId(int id);
    }
}
