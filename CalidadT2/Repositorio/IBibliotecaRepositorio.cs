﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorio
{
    public interface IBibliotecaRepositorio
    {
        List<Biblioteca> listaBiblioteca(Usuario usuario);
        void AgregarBiblioteca(int libro, Usuario usuario);
        void bibiotecaId(int libroId, Usuario usuario);
        void terminado(int libroId, Usuario usuario);
    }
}
