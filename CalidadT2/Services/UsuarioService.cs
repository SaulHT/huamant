﻿using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public class UsuarioService : IUsuarioRepositorio
    {
        private AppBibliotecaContext app;
       
        public UsuarioService(AppBibliotecaContext app)
        {
            this.app = app;
             
        }
        public Usuario getUsuario(string username, string password)
        {
           var usuario= app.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();

            return usuario;
        }

        public Usuario logeado(Claim claim)
        {
            var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();

            return user;
        }

        //public Usuario logeoUsuario()
        //{
        //     var claim = contex.User.Claims.FirstOrDefault();

        //     var user = logeado(claim);
        //    //var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
        //    return user;
        //}
    }
}
