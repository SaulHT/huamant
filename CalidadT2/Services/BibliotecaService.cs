﻿using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public class BibliotecaService : IBibliotecaRepositorio
    {
        private AppBibliotecaContext app;

        public BibliotecaService(AppBibliotecaContext app)
        {
            this.app = app;
        }

        public void AgregarBiblioteca(int libro, Usuario usuario)
        {
            var biblioteca = new Biblioteca
            {
                LibroId = libro,
                UsuarioId = usuario.Id,
                Estado = ESTADO.POR_LEER
            };

            app.Bibliotecas.Add(biblioteca);
            app.SaveChanges();
        }

        public List<Biblioteca> listaBiblioteca(Usuario usuario)
        {
            var model = app.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == usuario.Id)
                .ToList();
            return model;
        }

        public void terminado(int libroId, Usuario usuario)
        {
            var libro = app.Bibliotecas
                .Where(o => o.LibroId == libroId && o.UsuarioId == usuario.Id)
                .FirstOrDefault();

            libro.Estado = ESTADO.TERMINADO;
            app.SaveChanges();
        }

        public void bibiotecaId(int libroId, Usuario usuario)
        {
            var libro = app.Bibliotecas
                .Where(o => o.LibroId == libroId && o.UsuarioId == usuario.Id)
                .FirstOrDefault();

            libro.Estado = ESTADO.LEYENDO;
            app.SaveChanges();
        }

       
    }
}
