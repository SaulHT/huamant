﻿using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public class LibrosService : ILibrosRepositorio
    {
        private AppBibliotecaContext app;
        public LibrosService(AppBibliotecaContext app)
        {
            this.app = app;

        }

        public Libro libroId(int id)
        {
            var model = app.Libros
                .Include("Autor")
                .Include("Comentarios.Usuario")
                .Where(o => o.Id == id)
                .FirstOrDefault();

            return model;
        }

        public List<Libro> listaLibros()
        {
           var libro= app.Libros.Include(o => o.Autor).ToList();
            return libro;
        }
    }
}
