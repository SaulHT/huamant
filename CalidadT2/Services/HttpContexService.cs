﻿using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public class HttpContexService : IHttpContexRepositorio
    {
        private HttpContext context;
   
        public void SetHttpContext(HttpContext context)
        {
            this.context = context;
            
        }

        public void Login(ClaimsPrincipal claims)
        {
            context.SignInAsync(claims);
        }

        //public void LogeadoUser(Claim claim)
        //{
        //   context.User.Claims.FirstOrDefault();
           
        //}
    }
}
