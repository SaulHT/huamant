﻿using CalidadT2.Models;
using CalidadT2.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public class ComentarioService : IComentarioRepositorio
    {
        private readonly AppBibliotecaContext app;

        public ComentarioService(AppBibliotecaContext app)
        {
            this.app = app;
        }
        public void agregacomentario(Comentario comentario, Usuario usuario)
        {
            comentario.UsuarioId = usuario.Id;
            comentario.Fecha = DateTime.Now;
            app.Comentarios.Add(comentario);

            var libro = app.Libros.Where(o => o.Id == comentario.LibroId).FirstOrDefault();
            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;

            app.SaveChanges();
        }
    }
}
