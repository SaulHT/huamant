﻿using System;
using System.Linq;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly AppBibliotecaContext app;
        private ILibrosRepositorio Ilibro;
        private IComentarioRepositorio IComenta;
        private IUsuarioRepositorio Iuser;
        public LibroController(AppBibliotecaContext app, ILibrosRepositorio Ilibro, IComentarioRepositorio IComenta, IUsuarioRepositorio Iuser)
        {
            this.app = app;
            this.Ilibro = Ilibro;
            this.IComenta = IComenta;
            this.Iuser = Iuser;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
           
            var model = Ilibro.libroId(id);
            return View(model);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            //Usuario user = Iuser.logeoUsuario();
            Usuario user = LoggedUser();

            IComenta.agregacomentario(comentario,user);

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        private Usuario LoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
             var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            
            return user;
        }
    }
}
