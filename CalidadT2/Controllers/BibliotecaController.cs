﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Operations;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly AppBibliotecaContext app;
        private IBibliotecaRepositorio IBiblio;
        private IUsuarioRepositorio Iuser;
        private IHttpContexRepositorio IHttp;

        public BibliotecaController(AppBibliotecaContext app,IBibliotecaRepositorio IBiblio,IUsuarioRepositorio Iuser, IHttpContexRepositorio IHttp)
        {
            this.app = app;
            this.IBiblio = IBiblio;
            this.Iuser = Iuser;
            this.IHttp = IHttp;
        }

        [HttpGet]
        public IActionResult Index()
        {
             Usuario user = LoggedUser();
             //Usuario user = Iuser.logeoUsuario();
           
         
            
            var model = IBiblio.listaBiblioteca(user);

            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            Usuario user = LoggedUser();
          //  Usuario user = Iuser.logeoUsuario();
           
            IBiblio.AgregarBiblioteca( libro, user);

            TempData["SuccessMessage"] = "Se añádio el libro a su biblioteca";

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            Usuario user = LoggedUser() ;
            //Usuario user = Iuser.logeoUsuario();

            IBiblio.bibiotecaId(libroId,user);

            

            TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Usuario user = LoggedUser(); 
           //Usuario user = Iuser.logeoUsuario(); 

            
            IBiblio.terminado(libroId, user);

            TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        private Usuario LoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();

            //var user = Iuser.logeado(claim);
           var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}

