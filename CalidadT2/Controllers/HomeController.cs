﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using CalidadT2.Repositorio;

namespace CalidadT2.Controllers
{
    public class HomeController : Controller
    {
        private AppBibliotecaContext app;
        private ILibrosRepositorio Ilibro;
        public HomeController(AppBibliotecaContext app,ILibrosRepositorio Ilibro)
        {
            this.app = app;
            this.Ilibro = Ilibro;
        }

        [HttpGet]
        public IActionResult Index()
        {
            //var model = app.Libros.Include(o => o.Autor).ToList();
            var model = Ilibro.listaLibros();
            return View(model);
        }
    }
}
